import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class TicketService {
  private cartData = new BehaviorSubject<any[]>([]);
  constructor() {}
  updateCart(data: any[]) {
    this.cartData.next(data);
  }
  getCart(): Observable<any[]> {
    return this.cartData.asObservable();
  }
}
