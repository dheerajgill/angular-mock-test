import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppTicketComponent } from './ticket.component';

const routes: Routes = [
  {
    path: '',
    component: AppTicketComponent,
  },
];
@NgModule({
  declarations: [AppTicketComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes)],
  providers: [],
  entryComponents: [],
})
export class AppTicketModule {}
