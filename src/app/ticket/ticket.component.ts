import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TicketService } from '../services/ticket.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css'],
})
export class AppTicketComponent implements OnInit {
  ticketData: any[] = [
    {
      id: 1,
      title: 'Free Ticket',
      description: `Free ticket for anyone to make a valuable contribution towards our
    future online events programme. Thank you.`,
      info: 'Sales end in 1 hour',
      price: 0,
      count: 0,
    },
    {
      id: 2,
      title: 'Alumni VIP Ticket',
      description: `This livestream will broadcast via a private YouTube link that will
    be sent to ticket purchasers an hour prior to showtime.`,
      info: 'Sales end in 5 days',
      price: 3500,
      count: 0,
    },
  ];
  cartData: any[] = [];
  cartDetail: any = {};
  isInitialize: boolean = false;
  constructor(private router: Router, private ticketService: TicketService) {}

  ngOnInit(): void {
    this.ticketService.getCart().subscribe((res: any[]) => {
      this.cartData = res;
      this.getCartDetail();
      if (!this.isInitialize) {
        this.checkInCart();
      }
    });
  }
  checkInCart() {
    this.isInitialize = true;
    this.cartData.forEach((obj: any) => {
      let _find = this.ticketData.find((ele: any) => {
        return ele.id == obj.id;
      });
      if (_find) {
        _find.count = obj.count;
      }
    });
  }
  goToCheckout() {
    this.router.navigate(['/checkout']);
  }
  addTocart(item: any) {
    let _idx = this.cartData.findIndex((obj: any) => {
      return obj.id == item.id;
    });
    if (item.count == 0) {
      this.cartData.splice(_idx, 1);
    } else {
      if (_idx == -1) {
        this.cartData.push(item);
      } else {
        this.cartData[_idx] = item;
      }
    }
    this.ticketService.updateCart(this.cartData);
  }
  getCartDetail() {
    let subTotal: any = 0;
    this.cartData.forEach((obj: any) => {
      subTotal = subTotal + obj.count * obj.price;
    });
    let tax = (subTotal * 18) / 100;
    let total = subTotal + tax;
    this.cartDetail = { subTotal: subTotal, tax: tax, total: total };
  }
}
