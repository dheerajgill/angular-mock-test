import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { TicketService } from '../services/ticket.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class AppCheckoutComponent implements OnInit {
  cartData: any[] = [];
  cartDetail: any = {};
  formArr: any[] = [];
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private ticketService: TicketService
  ) {}

  ngOnInit(): void {
    this.ticketService.getCart().subscribe((res: any[]) => {
      this.cartData = res;
      this.getCartDetail();
      this.generateTicketForm();
    });
  }

  goToBack() {
    this.router.navigate(['/']);
  }
  getCartDetail() {
    let subTotal: any = 0;
    this.cartData.forEach((obj: any) => {
      subTotal = subTotal + obj.count * obj.price;
    });
    let tax = (subTotal * 18) / 100;
    let total = subTotal + tax;
    this.cartDetail = { subTotal: subTotal, tax: tax, total: total };
  }
  generateTicketForm() {
    this.cartData.forEach((obj: any) => {
      for (let index = 0; index < obj.count; index++) {
        this.addForm(obj);
      }
    });
  }
  addForm(data: any) {
    if (data.price == 0) {
      this.formArr.push({
        title: data.title,
        isFree: true,
        formGroup: this.fb.group({
          firstName: [, Validators.required],
          lastName: [, Validators.required],
          email: [
            ,
            Validators.compose([Validators.required, Validators.email]),
          ],
          brand: [],
        }),
      });
    } else {
      this.formArr.push({
        title: data.title,
        isFree: false,
        formGroup: this.fb.group({
          firstName: [, Validators.required],
          lastName: [, Validators.required],
          email: [
            ,
            Validators.compose([Validators.required, Validators.email]),
          ],
          jobTitle: [, Validators.required],
          company: [, Validators.required],
          country: ['', Validators.required],
        }),
      });
    }
  }
}
