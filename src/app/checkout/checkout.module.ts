import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppCheckoutComponent } from './checkout.component';

const routes: Routes = [
  {
    path: '',
    component: AppCheckoutComponent,
  },
];
@NgModule({
  declarations: [AppCheckoutComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [],
  entryComponents: [],
})
export class AppCheckoutModule {}
